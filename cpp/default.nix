{ pkgs }:
with pkgs; mkShell {
  packages = [
    clang-tools_16
    clang
    ninja
    cmake
  ];

  buildInputs = [
  ];

  CMAKE_EXPORT_COMPILE_COMMANDS = 1;

  WORDLIST = "${pkgs.scowl}/lib/aspell/en-common.wl";
}
