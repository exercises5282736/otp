#include<iostream>

template<typename T>
struct Optional {
  private:
    bool isEmpty;
    T value;
  public:
    Optional() : isEmpty(true), value(0) {}
    Optional(T value) : value(value) {};
  
    bool empty() {
      return isEmpty;
    }

    T inner() {
      return value;
    }
};

int main(int argc, char *argv[]) {
  Optional<int> n;
  Optional<int> s(4);

  std::cout << "n=" << n.empty() << ", s=" << s.empty() << std::endl;
  std::cout << "n=" << n.inner() << ", s=" << s.inner() << std::endl;

  return 0;
}
