#include <fstream>
#include <iostream>
#include <random>

const unsigned char MAX_ERRORS = 6;

int rand(int from, int to);

int main(int argc, char *argv[]) {
  char line_buffer[16];

  {
    std::ifstream words;
    words.open("words");

    int word_count = 0;
    while (!words.eof()) {
      words.getline(line_buffer, 16);
      word_count++;
    }

    words.clear();
    words.seekg(0);

    int word_i = rand(1, word_count);

    for (int i = 0; i < word_i; i++) {
      words.getline(line_buffer, 16);
    }

    words.close();
  }

  std::string word(line_buffer);
  std::string matches(word.length(), '_');

  unsigned char errors = 0;
  unsigned char guess;

  while (errors < MAX_ERRORS && matches.find('_') != std::string::npos) {
    std::cout << matches << std::endl;
    std::cout << "guess? ";
    std::cin >> guess;
    std::cout << std::endl;
    bool error = true;
    using iter = std::string::iterator;
    for (iter itw = word.begin(), itm = matches.begin(); itw != word.end();
         itw++, itm++) {
      if (tolower(*itw) == guess) {
        *itm = *itw;
        error = false;
      }
    }
    if (error)
      errors++;
  }

  std::cout << word << std::endl;
  if (errors < MAX_ERRORS)
    std::cout << "you win!\n";
  else
    std::cout << "you lose!\n";

  return 0;
}

int rand(int from, int to) {
  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_int_distribution<> distr(from, to);
  return distr(gen);
}
