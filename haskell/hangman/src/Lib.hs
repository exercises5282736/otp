module Lib
    ( Game
    , Status(..)
    , newGame
    , gameStep
    , hangman
    ) where

import Data.Function (fix)
import Data.List.NonEmpty (NonEmpty)
import Data.Semigroup (sconcat)

maxErrors :: Int
maxErrors = 6

data Status = Continuing | Lost | Won

data Game = Game { errors :: Int, game :: Hangman }

gameStep :: Game -> Char -> (String, Status, Game)
gameStep Game { errors = es, game = Hangman f } guess =
    case f guess of
        (State ms _ True, game') -> (ms, Won, Game es game')
        (State ms True _, game') -> (ms, Continuing, Game es game')
        (State ms _ _, game') | es < maxErrors ->
            (ms, Continuing, Game (es + 1) game')
        (State ms _ _, game') -> (ms, Lost, Game es game')

newGame :: Hangman -> Game
newGame h = Game { errors = 0, game = h }

data State = State {
    matches :: String,
    match :: Bool,
    won :: Bool
}

instance Semigroup State where
    State ams am aw <> State bms bm bw =
        State { matches = ams <> bms
              , match = am || bm
              , won = aw && bw }

newtype Hangman = Hangman (Char -> (State, Hangman))

hangman :: NonEmpty Char -> Hangman
hangman word = sconcat $ trivial_hangman <$> word

trivial_hangman :: Char -> Hangman
trivial_hangman c = Hangman $ fix $ \f g ->
    if c == g
    then ( State (c:[]) True True, Hangman $ wonF)
    else (State ('_':[]) False False, Hangman $ f)
    where
        wonF = fix (\f' _ -> (State (c:[]) False True, Hangman $ f'))

instance Semigroup Hangman where
    Hangman a <> Hangman b = Hangman $ \c ->
        let (as, ah) = a c
            (bs, bh) = b c
        in (as <> bs, ah <> bh)
