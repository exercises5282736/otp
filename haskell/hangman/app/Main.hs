module Main (main) where

import Data.List.NonEmpty (NonEmpty, nonEmpty, toList)
import Lib
import System.IO
import System.Random (newStdGen, randomR)

main :: IO ()
main = do
  hSetBuffering stdin NoBuffering
  wordList <- readFile "words" >>= return . lines
  word <- choose wordList
  case word of
    Nothing -> error "foo"
    Just w -> do
      gameLoop $ newGame $ hangman w
      putStrLn $ toList w

gameLoop :: Game -> IO ()
gameLoop game = do
    putStr "guess: "
    hFlush stdout
    guess <- getChar
    putChar '\n'
    let (matches, status, game') = gameStep game guess
    putStrLn $ matches ++ "\n"
    case status of
      Continuing -> gameLoop game'
      Lost -> putStrLn "you lose!"
      Won -> putStrLn "you win!"

choose :: [String] -> IO (Maybe (NonEmpty Char))
choose ws = choice
    where
        count = length ws
        choice = do
            rng <- newStdGen
            return . nonEmpty $ ws !! (fst $ randomR (0, count - 1) rng)

