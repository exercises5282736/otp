use std::fs::{read_to_string, File};
use std::io::{BufWriter, Write};
use std::path::Path;
use std::{env, iter};

fn main() {
    let data_rs_path = Path::new(&env::var("OUT_DIR").unwrap()).join("data.rs");
    let mut data_rs = BufWriter::new(File::create(data_rs_path).unwrap());

    // generate dictionary
    {
        let mut dict_set = phf_codegen::Set::new();

        read_to_string(Path::new(&env::var("WORDLIST").unwrap()))
            .unwrap()
            .lines()
            .map(|line| line.trim().to_lowercase())
            .filter(|line| !line.is_empty())
            .for_each(|word| {
                dict_set.entry(word.to_string());
            });

        writeln!(
            &mut data_rs,
            "pub static DICTIONARY: phf::Set<&'static str> = {};",
            dict_set.build()
        )
        .unwrap();
    }

    // generate tile data
    {
        let mut tiles = Vec::new();
        let mut tile_values = phf_codegen::Map::new();
        let mut tiles_tsv = csv::ReaderBuilder::new()
            .has_headers(false)
            .delimiter(b'\t')
            .from_path("assets/tiles.tsv")
            .unwrap();
        let mut tile_records = tiles_tsv.records();
        while let Some(Ok(record)) = tile_records.next() {
            let char = record.get(0).unwrap().chars().next().unwrap();
            let count = record.get(1).unwrap().parse::<usize>().unwrap();
            let value = record.get(2).unwrap();
            tiles.extend(iter::repeat(char).take(count));
            tile_values.entry(char, value);
        }
        writeln!(
            &mut data_rs,
            "pub static TILES: [char; {}] = [{}];",
            tiles.len(),
            tiles
                .iter()
                .map(|char| format!("'{}'", char.to_string()))
                .collect::<Vec<String>>()
                .join(", ")
        )
        .unwrap();
        writeln!(
            &mut data_rs,
            "pub static TILE_VALUES: phf::Map<char, u16> = {};",
            tile_values.build()
        )
        .unwrap();
    }

    // generate board layout
    {
        writeln!(
            &mut data_rs,
            "pub static BOARD_LAYOUT: [[FieldType; 15]; 15] = ["
        )
        .unwrap();
        for line in read_to_string("assets/board.ascii").unwrap().lines() {
            let fields = line.bytes();
            write!(&mut data_rs, "    [").unwrap();
            for field in fields {
                write!(
                    &mut data_rs,
                    "FieldType::{},",
                    match field {
                        b'.' => "CharX1",
                        b'2' => "CharX2",
                        b'3' => "CharX3",
                        b'W' => "WordX3",
                        b'w' => "WordX2",
                        _ => panic!("invalid character in board layout: {field}"),
                    }
                )
                .unwrap();
            }
            writeln!(&mut data_rs, "],").unwrap();
        }
        writeln!(&mut data_rs, "];").unwrap();
    }

    println!("cargo:rerun-if-changed=build.rs");
}
