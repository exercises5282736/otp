{
  inputs = {
    nixpkgs.url      = "github:NixOS/nixpkgs/nixos-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
    flake-utils.url  = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, rust-overlay, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
        words = pkgs.fetchzip {
          name = "scrabble-words";
          url = "https://users.cs.northwestern.edu/~robby/uc-courses/22001-2008-winter/sowpods.zip";
          hash = "sha256-y7PRct2eQSjZG9q7uswFIeZvtokwYXUtN6/ikACI/7Y=";
        };
      in
      with pkgs;
      {
        devShells.default = mkShell rec {
          nativeBuildInputs = [
            pkg-config
          ];

          buildInputs = [
            (rust-bin.fromRustupToolchainFile ./rust-toolchain.toml)
          ];

          LD_LIBRARY_PATH = lib.makeLibraryPath buildInputs;

          WORDLIST = "${words}/sowpods.txt";

          # shellHook  = 
          #   ''
          #     curl https://users.cs.northwestern.edu/~robby/uc-courses/22001-2008-winter/sowpods.zip | zcat > words
          #     export WORDLIST="$PWD/assets/words"
          #   '';
        };
      }
    );
}
