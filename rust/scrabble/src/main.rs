mod data;

use anyhow::Result;

fn is_known_word(word: &str) -> bool {
    data::DICTIONARY.contains(word)
}

fn find_all_matches(tiles: Vec<char>) -> impl Iterator<Item = &'static str> {
    data::DICTIONARY
        .iter()
        .filter_map(move |word| find_match(&tiles, word).map(|_| *word))
}

fn find_match(tiles: &Vec<char>, word: &str) -> Option<String> {
    let mut remaining = tiles.clone();
    let mut result = String::with_capacity(word.len());
    for char in word.chars() {
        if let Some(match_pos) = remaining.iter().position(|tile| char == *tile) {
            result.push(remaining.swap_remove(match_pos));
        } else if let Some(match_pos) = remaining.iter().position(|tile| '*' == *tile) {
            result.push(remaining.swap_remove(match_pos));
        } else {
            return None;
        }
    }
    return Some(result);
}

fn compute_score(word: &str) -> u16 {
    word.chars()
        .map(|char| data::TILE_VALUES.get(&char).unwrap())
        .sum()
}

fn main() -> Result<()> {
    println!(
        "{:?}",
        find_all_matches(vec!['e', 'e', 'o', 's', 'k', '*']).collect::<Vec<&str>>()
    );
    println!("{:?}", is_known_word("telephone"));
    println!("{:?}", compute_score("quandary"));
    println!("{:?}", compute_score("t*p"));
    Ok(())
}
