include!(concat!(env!("OUT_DIR"), "/data.rs"));

enum FieldType {
    CharX1,
    CharX2,
    CharX3,
    WordX2,
    WordX3,
}
