# LINUX SYSCALLS
#
# id:   %rax
# args: %rdi %rsi %rdx
# ret:  %rax

.set SC_READ,        0  # fd, buf, count -> bytes read
.set SC_WRITE,       1  # fd, buf, count -> bytes written
.set SC_OPEN,        2  # path, flags, mode
.set SC_CLOSE,       3  # fd
.set SC_EXIT,       60  # error_code
.set SC_GETRANDOM, 318  # buf, count, flags -> bytes read

.set FD_STDIN, 0
.set FD_STDOUT, 1

.set BUFFER_SIZE, 4096

.set MAX_ERRORS, 6

.global _start

.text
_start:
        call count_words
        call rand
        call find_word
        call start_game

        mov $SC_EXIT, %rax
        mov $0, %rdi
        syscall

# Ask for guesses until win or lose condition is reached.
# Expect secret word in `word` and word length in %rcx
start_game:
        push %rcx
        vzeroall                  # zero SIMD registers

        movdqu word, %xmm9        # secret word (guesses in %xmm8)
        mov $97, %rax
        movq %rax, %xmm1
        vpbroadcastb %xmm1, %xmm0
        vpcmpgtb %xmm9, %xmm0, %xmm1
        vpcmpgtb %xmm6, %xmm9, %xmm0
        pand %xmm0, %xmm1
        mov $32, %rax
        movq %rax, %xmm2
        vpbroadcastb %xmm2, %xmm0
        pand %xmm1, %xmm0
        paddb %xmm0, %xmm9

        mov $95, %rax
        movq %rax, %xmm0
        vpbroadcastb %xmm0, %xmm7 # underscores

        push $0                   # init error count

_game_loop:
        vpcmpgtb %xmm6, %xmm9, %xmm1
        pand %xmm7, %xmm1
        vpcmpeqb %xmm6, %xmm8, %xmm2
        pand %xmm1, %xmm2
        por %xmm8, %xmm2

        movdqu %xmm2, matches
        mov $SC_WRITE, %rax
        mov $FD_STDOUT, %rdi
        mov $matches, %rsi
        mov 8(%rsp), %rdx
        syscall

        mov $10, %rdi
        call write_char

        mov $SC_WRITE, %rax
        mov $FD_STDOUT, %rdi
        mov $prompt, %rsi
        mov $prompt_len, %rdx
        syscall

_game_loop_read_char:
        call read_char
        mov char, %rax
        cmp $10, %rax
        je _game_loop_read_char

        movq char, %xmm1
        mov $10, %rdi
        call write_char

        vpbroadcastb %xmm1, %xmm0
        vpcmpeqb %xmm0, %xmm9, %xmm1
        pand %xmm9, %xmm1
        por %xmm1, %xmm8

        movq %xmm1, %rax
        movhlps %xmm1, %xmm1
        movq %xmm1, %rbx
        or %rbx, %rax
        cmp $0, %rax
        jne _game_loop_write_matches
        mov (%rsp), %rax
        add $1, %rax
        mov %rax, (%rsp)

_game_loop_write_matches:
        vpcmpeqb %xmm9, %xmm8, %xmm1
        movq %xmm1, %rax
        movhlps %xmm1, %xmm1
        movq %xmm1, %rbx
        not %rax
        not %rbx
        or %rbx, %rax
        cmp $0, %rax
        je _game_win
        mov (%rsp), %rax
        cmp $MAX_ERRORS, %rax
        jge _game_loss
        jmp _game_loop

_game_win:
        mov $SC_WRITE, %rax
        mov $FD_STDOUT, %rdi
        mov $word, %rsi
        mov 8(%rsp), %rdx
        syscall

        mov $SC_WRITE, %rax
        mov $FD_STDOUT, %rdi
        mov $msg_won, %rsi
        mov $msg_won_len, %rdx
        syscall

        jmp _game_end

_game_loss:
        movdqu %xmm2, matches
        mov $SC_WRITE, %rax
        mov $FD_STDOUT, %rdi
        mov $word, %rsi
        mov 8(%rsp), %rdx
        syscall

        mov $SC_WRITE, %rax
        mov $FD_STDOUT, %rdi
        mov $msg_lost, %rsi
        mov $msg_lost_len, %rdx
        syscall

_game_end:
        pop %rax
        pop %rcx
        ret

# Count lines in word file.
# Return word count in %ebx.
count_words:
        mov %rsp, %rax            # save current stack pointer
        add $BUFFER_SIZE, %rsp    # allocate buffer on stack
        push %rax                 # save buffer address

        mov $SC_OPEN, %rax
        mov $words_file, %rdi
        mov $0, %rsi
        mov $0, %rdx
        syscall
        mov %rax, %rdi

        xor %rbx, %rbx

_count_words_read_block:
        leaq 8(%rsp), %rsi
        mov $SC_READ, %rax
        mov $BUFFER_SIZE, %rdx
        syscall

        mov %rax, %rdx
        add %rsi, %rdx   # buffer start + bytes read

_count_words_block_loop:
        cmpb $10, (%rsi)
        jne _count_words_block_loop_inc
        add $1, %rbx

_count_words_block_loop_inc:
        addq $1, %rsi
        cmp %rsi, %rdx
        jne _count_words_block_loop

        cmp $BUFFER_SIZE, %rax   # eof?
        je _count_words_read_block

        mov $SC_CLOSE, %rax
        syscall

        pop %rsp
        ret

# Find %rdx'th word in word file.
# Return word in `word` and word length in %rcx.
find_word:
        mov %rsp, %rax            # save current stack pointer
        add $BUFFER_SIZE, %rsp    # allocate buffer on stack
        push %rax                 # save buffer address

        mov %rdx, %rbx            # keep n in %rbx

        mov $SC_OPEN, %rax
        mov $words_file, %rdi
        mov $0, %rsi
        mov $0, %rdx
        syscall
        mov %rax, %rdi

        xor %rcx, %rcx            # reset word length

_find_word_read_block:
        leaq 8(%rsp), %rsi
        mov $SC_READ, %rax
        mov $BUFFER_SIZE, %rdx
        push %rcx
        syscall
        pop %rcx

        mov %rax, %rdx
        add %rsi, %rdx   # buffer start + bytes read

_find_word_block_loop:
        cmp $0, %rbx
        jne _find_word_block_loop_skip_word
        cmpb $10, (%rsi)
        je _find_word_done
        push %rax
        push %rbx
        lea word(%rcx), %rax
        add $1, %rcx
        movb (%rsi), %bl
        movb %bl, (%rax)
        pop %rbx
        pop %rax

_find_word_block_loop_skip_word:
        cmpb $10, (%rsi)
        jne _find_word_block_loop_inc
        sub $1, %rbx

_find_word_block_loop_inc:
        addq $1, %rsi
        cmp %rsi, %rdx
        jne _find_word_block_loop

        cmp $BUFFER_SIZE, %rax   # eof?
        je _find_word_read_block

_find_word_done:
        push %rcx
        mov $SC_CLOSE, %rax
        syscall
        pop %rcx

        pop %rsp
        ret

# Find random u32 between 0 and %rbx - 1.
# We use the `getrandom` syscall to seed an LCG.
# Return random number in %rdx.
rand:
        mov $SC_GETRANDOM, %rax
        lea -8(%rsp), %rdi
        mov $8, %rsi
        xor %rdx, %rdx
        syscall

        xor %rax, %rax
        xor %rdx, %rdx
        mov (%rdi), %rsi
        mov $214013, %rax   # a
        mul %rsi            # a * r0 
        add $2531011, %rax  # a * r0 + c
        xor %rdx, %rdx
        div %rbx
        ret

write_char:
        mov %rdi, char
        mov $SC_WRITE, %rax
        mov $FD_STDOUT, %rdi
        mov $char, %rsi
        mov $1, %rdx
        syscall
        ret

read_char:
        mov $SC_READ, %rax
        mov $FD_STDIN, %rdi
        mov $char, %rsi
        mov $1, %rdx
        syscall
        ret

.data
words_file:
        .string "words"
prompt:
        .ascii "\nguess? "
        prompt_len = (. - prompt)
msg_won:
        .ascii "\nyou win!\n"
        msg_won_len = (. - msg_won)
msg_lost:
        .ascii "\nyou lose!\n"
        msg_lost_len = (. - msg_lost)

.bss
        .lcomm char, 1
        .lcomm word, 16
        .lcomm matches, 16
