.set E_TOO_FEW_ARGS, 1
.set E_ARG_IS_ZERO,  2

.global _start

.text
_start:
        # verify min arg count
        mov $E_TOO_FEW_ARGS, %rdi
        mov (%rsp), %rax # argc
        cmp $2, %rax
        jl error

        # atoi(args[1][0..3])
        #
        # We assume without checking, that the first arg is at least 4 bytes
        # long and contains only ASCII digits. If these conditions are not met
        # the program might loop indefinitely.
        mov $2, %rbx # index (0: argc, 1: argv[0], 2: argv[1])
        lea (%rsp, %rbx, 8), %rax # scale factor 8 because we are in 64bit mode
        mov (%rax), %rax # deref stack pointer
        mov (%rax), %ebx # get first 4 bytes of first arg
        xor %eax, %eax
        mov $10, %esi
        sub $48, %bl
        sub $48, %bh
        movzx %bl, %eax
        mul %esi
        movzx %bh, %edi
        add %edi, %eax
        shr $16, %ebx
        sub $48, %bl
        sub $48, %bh
        mul %esi
        movzx %bl, %edi
        add %edi, %eax
        mul %esi
        movzx %bh, %edi
        add %edi, %eax

        # verify arg non zero
        mov $E_ARG_IS_ZERO, %rdi
        cmp $0, %eax
        je error

loop:
        # preserve input for detecting fixed point
        mov %eax, %edi

        # split digits: ch, cl, bh, bl
        xor %edx, %edx
        mov $10, %esi
        div %esi
        mov %dl, %ch

        xor %edx, %edx
        mov $10, %esi
        div %esi
        mov %dl, %cl

        xor %edx, %edx
        mov $10, %esi
        div %esi
        mov %dl, %bh

        xor %edx, %edx
        mov $10, %esi
        div %esi
        mov %dl, %bl

        # detect fixed point (kaprekar's constant 6174)
        cmp $6174, %edi
        je stop

        # sort digits using sorting network
        #
        #     1 2 3 4
        # ch -*---*---
        # cl -|-*-*-*-
        # bh -*-|-*-*-
        # bl ---*-*---
        # 
        # ch > cl > bh > bl
        cmp %ch, %bh
        jl __sort_skip_1
        xchg %ch, %bh
__sort_skip_1:
        cmp %cl, %bl
        jl __sort_skip_2
        xchg %cl, %bl
__sort_skip_2:
        cmp %ch, %cl
        jl __sort_skip_3a
        xchg %ch, %cl
__sort_skip_3a:
        cmp %bh, %bl
        jl __sort_skip_3b
        xchg %bh, %bl
__sort_skip_3b:
        cmp %cl, %bh
        jl __sort_skip_4
        xchg %cl, %bh
__sort_skip_4:

        # join to numbers %ax and %bx
        mov $10, %rsi

        ## (bl, bh, cl, ch)
        movzx %bl, %eax
        mul %rsi
        movzx %bh, %edx
        add %edx, %eax
        mul %rsi
        movzx %cl, %edx
        add %edx, %eax
        mul %rsi
        movzx %ch, %edi
        add %eax, %edi

        ## (ch, cl, bh, bl)
        movzx %ch, %eax
        mul %rsi
        movzx %cl, %edx
        add %edx, %eax
        mul %rsi
        movzx %bh, %edx
        add %edx, %eax
        mul %rsi
        movzx %bl, %esi
        add %esi, %eax

        sub %edi, %eax
        jmp loop

stop:
        #convert digits to ASCII
        add $48, %ch
        add $48, %cl
        add $48, %bh
        add $48, %bl 
        
        # convert to string and print
        pushw $0x000A # push LF (\n)
        pushw %cx
        pushw %bx

        mov $1, %rax # write
        mov $1, %rdi # stdout
        mov %rsp, %rsi # stack as buffer
        mov $5, %rdx # 4 digits + 1 LF
        syscall

        mov %rbp, %rsp # restore stack pointer to base

        # exit 0
        mov $60, %rax
        mov $0, %rdi
        syscall

error:  # exit %rdi
        mov $60, %rax
        syscall
       
