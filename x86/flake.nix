{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.05";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShells.default = with pkgs; mkShell {
          packages = [
            clang-tools_16
            gdb
            binutils
          ];

          buildInputs = [
          ];

          WORDLIST = "${pkgs.scowl}/lib/aspell/en-common.wl";
        };
      }
    );
}
