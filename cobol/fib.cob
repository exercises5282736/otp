identification division.
program-id. Fibonacci.

data division.
working-storage section.
01  O  PIC zzzz value 0.
01  A  PIC 9(4) value 0.
01  B  PIC 9(4) value 1.
01  C  PIC 9(4).

procedure division.
display O.
perform fib-advance 16 times.
fib-advance.
	set C to A.
	set A to B.
	add A to C.
	set B to C.
	set O to A
	display O.
