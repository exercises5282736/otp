{ stdenv
, lib
, fetchurl
, gmp
, db
, ncurses
}:

stdenv.mkDerivation rec {
  pname = "gnu-cobol";
  version = "3.2";

  src = fetchurl {
    url = "https://sourceforge.net/projects/gnucobol/files/gnucobol/${version}/gnucobol-${version}.tar.gz";
    sha256 = "sha256-KfMKdxdgFYR/CvsuIpOeOXmLtNmMfHom9nZZMLRVPFI=";
  };

  buildInputs = [
    gmp
    db
    ncurses
  ];

  enableParallelBuilding = true;

  meta = with lib; {
    homepage = "https://gnucobol.sourceforge.io";
    description = "GNU Cobol";
    platforms = platforms.linux;
    mainProgram = "cobc";
  };
}