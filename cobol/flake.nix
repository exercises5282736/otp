{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.05";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        gnu-cobol = pkgs.callPackage ./gnu-cobol.nix {};
      in
      {
        packages.gnu-cobol = gnu-cobol;

        devShells.default = pkgs.mkShell {
          buildInputs = with pkgs; [
            scowl
            gmp
          ];

          packages = [
            gnu-cobol
          ];

          WORDLIST = "${pkgs.scowl}/lib/aspell/en-common.wl";
        };
      }
    );
}
