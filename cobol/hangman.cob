       IDENTIFICATION DIVISION.
       PROGRAM-ID. HANGMAN.

       ENVIRONMENT DIVISION.
          INPUT-OUTPUT SECTION.
             FILE-CONTROL.
             SELECT WORDLIST ASSIGN TO 'words'
             ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION.
          FILE SECTION.
          FD WORDLIST.
          01 FILE-WORD       PIC A(10).

          WORKING-STORAGE SECTION.
          01 RAND            PIC V9(6).
          01 WORD-INDEX      PIC 9(6).
          01 WORD-COUNT      PIC 9(6) VALUE 0.
          01 WORD            PIC A(10).
          01 WORD-LENGTH     PIC 9(2).
          01 MATCHES         PIC A(10).
          01 EOF             PIC 9(1) VALUE 0.
          01 I               PIC 9(6) VALUE 0.
          01 GUESS           PIC A(1).
          01 IS-ERROR        PIC 9(1).
          01 ERRORS          PIC 9(1) VALUE 0.
          01 MAX-ERRORS      PIC 9(1) VALUE 6. 
          01 GAME-OVER-MSG   PIC X(5).

          SCREEN SECTION.

          01 BLANK-SCREEN.
             05 BLANK SCREEN BACKGROUND-COLOR 1 FOREGROUND-COLOR 11.

          01 HANGMAN-SCREEN.
             05 BACKGROUND-COLOR 1 FOREGROUND-COLOR 11
                LINE 6 COLUMN 16 PIC A(10) FROM MATCHES.
             05 BACKGROUND-COLOR 1 FOREGROUND-COLOR 11
                LINE 8 COLUMN 8 VALUE IS 'errors:'.
             05 BACKGROUND-COLOR 1 FOREGROUND-COLOR 11
                LINE 8 COLUMN 16 PIC 9(1) FROM ERRORS.
             05 BACKGROUND-COLOR 1 FOREGROUND-COLOR 11
                LINE 9 COLUMN 9 VALUE IS 'guess:'.
             05 BACKGROUND-COLOR 1 FOREGROUND-COLOR 11
                LINE 9 COLUMN 16 PIC A(1) TO GUESS.

          01 RESOLUTION-SCREEN.
             05 BACKGROUND-COLOR 1 FOREGROUND-COLOR 11
                LINE 6 COLUMN 16 PIC A(10) FROM WORD.
             05 BACKGROUND-COLOR 1 FOREGROUND-COLOR 11
                LINE 8 COLUMN 8 VALUE IS 'errors:'.
             05 BACKGROUND-COLOR 1 FOREGROUND-COLOR 11
                LINE 8 COLUMN 16 PIC 9(1) FROM ERRORS.
             05 BACKGROUND-COLOR 1 FOREGROUND-COLOR 11
                LINE 10 COLUMN 16 PIC A(5) FROM GAME-OVER-MSG.
             05 BACKGROUND-COLOR 1 FOREGROUND-COLOR 11
                LINE 12 COLUMN 1 VALUE IS ' '.

       PROCEDURE DIVISION.
          OPEN INPUT WORDLIST.
             PERFORM UNTIL EOF = 1
                READ WORDLIST INTO WORD
                   AT END MOVE 1 TO EOF
                   NOT AT END ADD 1 TO WORD-COUNT
                END-READ
             END-PERFORM.
          CLOSE WORDLIST.
          MOVE FUNCTION RANDOM TO RAND.
          COMPUTE WORD-INDEX = WORD-COUNT * RAND.
          MOVE 0 TO EOF.
          OPEN INPUT WORDLIST.
             PERFORM UNTIL EOF = 1
                READ WORDLIST INTO WORD
                   AT END MOVE 1 TO EOF
                   NOT AT END
                      IF I = WORD-INDEX THEN
                         MOVE 1 TO EOF
                      ELSE
                         ADD 1 TO I
                      END-IF
                END-READ
             END-PERFORM.
          CLOSE WORDLIST.
          MOVE FUNCTION LOWER-CASE(WORD) TO WORD.
          INSPECT WORD TALLYING WORD-LENGTH FOR
             CHARACTERS BEFORE INITIAL ' '.
          MOVE ALL '_' TO MATCHES(1:WORD-LENGTH).
          PERFORM UNTIL MATCHES = WORD OR ERRORS = MAX-ERRORS
             DISPLAY BLANK-SCREEN
             DISPLAY HANGMAN-SCREEN
             ACCEPT HANGMAN-SCREEN
             MOVE 1 TO IS-ERROR
             MOVE 1 TO I
             PERFORM UNTIL I GREATER THAN WORD-LENGTH
               IF GUESS = WORD(I:1) THEN
                  MOVE GUESS TO MATCHES(I:1)
                  MOVE 0 TO IS-ERROR
               END-IF
               ADD 1 TO I
             END-PERFORM
             ADD IS-ERROR TO ERRORS
          END-PERFORM.
          IF ERRORS = MAX-ERRORS THEN
             MOVE 'lost!' TO GAME-OVER-MSG
          ELSE
             MOVE 'won!' TO GAME-OVER-MSG
          END-IF.
          DISPLAY BLANK-SCREEN.
          DISPLAY RESOLUTION-SCREEN.
       STOP RUN.
