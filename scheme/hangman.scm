(define-module (hangman))

(use-modules (ice-9 i18n)
             (ice-9 rdelim)
             (rnrs io ports))

(define words-file "hangman/words")

(define (make-game secret matches errors)
  `(game ,secret ,matches . ,errors))

(define game-secret cadr)
(define game-matches caddr)
(define game-errors cdddr)

(define (game-won? game)
  (equal? (game-secret game) (game-matches game)))

(define (game-lost? game)
  (> (game-errors game) 6))

(define (game-display game)
  (display (list->string (game-matches game)))
  (newline))

(define (game-tail game)
  (make-game (cdr (game-secret game))
             (cdr (game-matches game))
             (game-errors game)))

(define (game-advance game guess)
  (define (game-advance-rec game guess match?)
    (if (nil? (game-secret game))
        (make-game '() '()
          (if match? (game-errors game) (+ 1 (game-errors game))))
        (let* ((first-sec (car (game-secret game)))
               (first-mat (car (game-matches game)))
               (first-match? (eq? (char-locale-downcase first-sec) guess))
               (match? (or match? first-match?))
               (rest (game-advance-rec (game-tail game) guess match?)))
          (make-game (cons first-sec (game-secret rest))
                     (cons (if first-match? first-sec first-mat)
                           (game-matches rest))
                     (game-errors rest)))))
  (game-advance-rec game guess #f))

(define (words-count!)
  (define (count-lines! port n)
    (if (port-eof? port)
        n
        (begin
          (%read-line port)
          (count-lines! port (+ 1 n)))))
  (call-with-input-file words-file
    (lambda (port) (count-lines! port 0))))

(define (words-find! n)
  (define (words-find-rec! port n)
    (if (eq? 0 n)
        (car (%read-line port))
        (begin
          (%read-line port)
          (words-find-rec! port (- n 1)))))
  (call-with-input-file words-file
    (lambda (port) (words-find-rec! port n))))

(define (random-word!)
  (set! *random-state* (random-state-from-platform))
  (words-find! (random (words-count!))))

(define (new-game secret)
  (make-game (string->list secret)
             (make-list (string-length secret) #\_)
             0))

(define (read-char!)
  (let ((char (get-char (current-input-port))))
    (get-char (current-input-port)) ; consume LF
    char))

(define (game-play! game)
  (newline)
  (cond ((game-won? game) (display (list->string (game-secret game)))
                          (newline)
                          (display "you win!")
                          (newline))
        ((game-lost? game) (display (list->string (game-secret game)))
                           (newline)
                           (display "you lose!")
                           (newline))
        (#t (game-display game)
            (display "guess? ")
            (game-play!
              (game-advance game (read-char!))))))

(game-play! (new-game (random-word!)))