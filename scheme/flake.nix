{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.05";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShells.default = with pkgs; mkShell {
          packages = [
            guile
          ];

          buildInputs = [
          ];

          shellHook = ''
            export WORDLIST="${pkgs.scowl}/lib/aspell/en-common.wl"

            mkdir -p hangman
            grep -E "^[A-Za-z][a-z]{3,9}$" $WORDLIST > hangman/words
          '';

          LANG = "C";
        };
      }
    );
}
